#!/usr/bin/python3

import os
import re
import sys
import stat
import shutil
import pathlib
import argparse
import subprocess

import lddwrap

def concat_path(path1, path2):
    path1 = pathlib.Path(path1)
    path2 = pathlib.Path(path2)
    return path1 / path2.relative_to(path2.anchor)


def copy_with_deps(files, prefix):
    for f in files:
        dest_f = concat_path(prefix, f)
        os.makedirs(os.path.dirname(dest_f), exist_ok=True)
        shutil.copy(f, dest_f)
        print('cp {} {}'.format(f, dest_f))

        try:
            deps = lddwrap.list_dependencies(path=pathlib.Path(f))
        except:
            continue

        for dep in deps:
            if not dep.path:
                continue
            dep_f = concat_path(prefix, dep.path)
            os.makedirs(os.path.dirname(dep_f), exist_ok=True)
            shutil.copy(dep.path, dep_f) # FIXME don't replace?
            print('cp {} {}'.format(dep.path, dep_f))


def prepare_initramfs(kernel_version, prefix, init):
    prefix = pathlib.Path(prefix)
    print("Creating base directory layout...")
    dirs = 'bin dev etc lib lib64 mnt/root proc root sbin sys tmp'.split()
    for dir_ in dirs:
        os.makedirs(concat_path(prefix,  dir_))
        print(concat_path(prefix, dir_))
    os.symlink('lib', prefix / 'lib32')
    print('lib -> {}'.format(prefix / 'lib32'))

    shutil.copy(init, prefix / 'init')
    print('cp {} {}'.format(init, prefix/'init'))
    os.chmod(prefix / 'init', os.stat(prefix / 'init').st_mode | stat.S_IEXEC)
    print('chmod o+x {}'.format(prefix/'init'))

    # Resolve bash and busybox from PATH

    files = ['/bin/busybox', '/bin/bash']
    # Copy busybox and bash from PATH
    copy_with_deps(files, prefix)


def main():
    parser = argparse.ArgumentParser(
            description="create initramfs root")
    parser.add_argument('--init', '-i' , type=str, required=True)
    parser.add_argument('--target', '-t', type=str, required=True)
    parser.add_argument('--kernel', '-k', type=str, required=True)

    args = parser.parse_args()

    import pudb; pudb.set_trace()
    if not os.path.isfile(args.init):
        print('Provided init is not a file')
        sys.exit(1)
    match = re.compile('[0-9]+\.[0-9]+\.[0-9]+')
    if not match.match(args.kernel):
        print('kernel version seems wrong')
        sys.exit(1)

    print('Preparing initramfs for kernel $KERNEL')
    print('Making root directory: {}'.format(args.target))
    os.makedirs(args.target)

    prepare_initramfs(args.kernel, args.target, args.init)
    prefix = pathlib.Path(args.target)
    if not prefix.is_dir():
        print("Something weird is going on")
        sys.exit(1)
    subprocess.call(
        ['/usr/libexec/plymouth/plymouth-populate-initrd', '-v', '-t', "{}".format(prefix)])

if __name__ == '__main__':
    main()
